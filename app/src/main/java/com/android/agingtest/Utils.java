package com.android.agingtest;

import android.app.Activity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.util.Locale;

public class Utils {

    /**
     * 测试，日志总开关
     */
    public static boolean isDebug = false;

    /**
     * 计算时间
     *
     * @param seconds 秒数
     * @return 转换后的时间
     */
    public static String calculatePlayTime(int seconds) {
        int minute = seconds / 60;
        int second = seconds % 60;
        int hour = minute / 60;
        minute = minute % 60;
        if (hour > 0) {
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minute, second);
        }
        return String.format(Locale.getDefault(), "%02d:%02d", minute, second);
    }

    /**
     * 设置窗口亮度
     *
     * @param brightness 亮度
     */
    public static void setWindowBrightness(Activity activity, float brightness) {
        if (isDebug) {
            Log.w(activity.getClass().getSimpleName(), "设置亮度：setWindowBrightness: brightness：" + brightness);
        }
        Window window = activity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = brightness;
        window.setAttributes(lp);
    }

}
