package com.android.agingtest;

import android.app.Activity;
import android.app.Service;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class VideoActivity extends Activity {

    private static final String TAG = "VideoActivity测试：";
    private static final int MSG_SEEK = 4000;
    private static final int MSG_SHOW_PREVIEW = 4001;
    private static final int MSG_UPDATE_SEEK_BAR = 4002;
    public static VideoActivity videoActivity = null;
    // 记录当前系统音量
    private int restoreVolume = -1;
    private AudioManager mAudioManager;

    TextView remainingTime;
    VideoView videoView;
    SeekBar seekBar;
    TextView textViewTime;
    TextView textViewCurrentPosition;
    Uri videoUri;
    // String videoPath;
    ImageView playControlView;
    VideoHandler videoHandler;
    boolean isLoop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoActivity = this;
        setContentView(R.layout.activity_video);
        // 设置屏幕不休眠
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
        remainingTime = findViewById(R.id.remaining_time);
        videoView = findViewById(R.id.videoView);
        seekBar = findViewById(R.id.seekBar);
        textViewTime = findViewById(R.id.textViewTime);
        textViewCurrentPosition = findViewById(R.id.textViewCurrentPosition);
        playControlView = findViewById(R.id.playControlView);
        videoHandler = new VideoHandler(this);

        try {
            // videoPath = this.getIntent().getStringExtra("path");
            // videoView.setVideoURI(videoPath);

            videoUri = this.getIntent().getParcelableExtra("path");
            videoView.setVideoURI(videoUri);

            showPreview();
            isLoop = getIntent().getBooleanExtra("loop", false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 为进度条添加进度更改事件
        seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);

        //播放器准备就绪
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                seekBar.setMax(videoView.getDuration());
                seekBar.setProgress(0);
                textViewTime.setText(time(videoView.getDuration()));
                videoView.setBackground(null);
            }
        });

        // 在播放完毕被回调
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                textViewCurrentPosition.setText(time(videoView.getDuration()));
                playControlView.setVisibility(View.VISIBLE);
                seekBar.setProgress(0);
                textViewCurrentPosition.setText(time(0L));

                if (isLoop) {
                    playControlView.setVisibility(View.GONE);
                    play();
                }
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // "播放出错"
                playControlView.setVisibility(View.GONE);
                return false;
            }
        });

        // 点击视频进入暂停
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isDebug) {
                    Log.d(TAG, "videoView click");
                }
                if (videoView.isPlaying()) {
                    // MainActivity.startCountDown = false;
                    videoView.pause();
                    playControlView.setVisibility(View.VISIBLE);
                } else if (playControlView.getVisibility() == View.VISIBLE) {
                    playControlView.setVisibility(View.GONE);
                    videoView.start();
                }
            }
        });

        // 暂停时视频中间会出现一个播放按钮，按下它时，会继续播放，并隐藏此按钮
        playControlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isDebug) {
                    Log.d(TAG, "playControlView click");
                }
                playControlView.setVisibility(View.GONE);
                play();
            }
        });

        play();
        // 设置最大亮度和音量
        Utils.setWindowBrightness(this, WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
        setMaxVolume();
        videoHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 1000);
    }

    private void play() {
        videoView.setBackground(null);
        videoView.start();
        // MainActivity.startCountDown = true;
    }

    private void watchPlayDurationAndSetSeekBar() {
        if (videoView.isPlaying()) {
            int current = videoView.getCurrentPosition();
            seekBar.setProgress(current);
            textViewCurrentPosition.setText(time(videoView.getCurrentPosition()));
        }
    }

    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        // 当进度条停止修改的时候触发
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // 取得当前进度条的刻度
            int progress = seekBar.getProgress();
            Message msg = new Message();
            msg.what = MSG_SEEK;
            msg.obj = progress;
            videoHandler.sendMessage(msg);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }
    };

    private String time(long millionSeconds) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millionSeconds);
        return simpleDateFormat.format(c.getTime());
    }

    private void showPreview() {
        // Bitmap firstFrame = getFirstFrameDrawable(videoPath);
        Bitmap firstFrame = getFirstFrameDrawable(videoUri);
        if (firstFrame != null) {
            BitmapDrawable bd = new BitmapDrawable(getResources(), firstFrame);
            videoView.setBackground(bd);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView.isPlaying()) {
            videoView.pause();
            playControlView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.seekTo(seekBar.getProgress());
        if (!videoView.isPlaying()) {
            videoView.start();
            playControlView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Utils.isDebug) {
            Log.w(TAG, "onDestroy");
        }
        MainActivity.startCountDown = false;
        videoHandler.removeCallbacksAndMessages(null);
        // 恢复系统原来的音量亮度
        restoreSystemVolume();
        Utils.setWindowBrightness(this, WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE);
        videoActivity = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this)
                    .setTitle("是否确认退出测试？")
                    .setPositiveButton("确认退出", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("取消", null)
                    .show();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return true;
    }

    /**
     * 设置最大音量
     */
    private void setMaxVolume() {
        try {
            if (restoreVolume < 0) {
                restoreVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            }
            int volume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            if (Utils.isDebug) {
                volume = 5;
                Log.w(TAG, "设置最大音量: restoreVolume:" + restoreVolume + ", MaxVolume:" + volume);
            }
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_PLAY_SOUND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 恢复系统音量
     */
    private void restoreSystemVolume() {
        if (restoreVolume < 0) {
            return;
        }
        try {
            if (Utils.isDebug) {
                Log.w(TAG, "恢复系统音量: restoreVolume:" + restoreVolume);
            }
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, restoreVolume, AudioManager.FLAG_PLAY_SOUND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Bitmap getFirstFrameDrawable(Uri uri) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            mmr.setDataSource(this, uri);
            return mmr.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected Bitmap getFirstFrameDrawable(String vPath) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            mmr.setDataSource(vPath);
            return mmr.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    class VideoHandler extends StaticHandler<VideoActivity> {

        public VideoHandler(VideoActivity videoActivity) {
            super(videoActivity);
        }

        @Override
        protected void handleMessage(Message msg, VideoActivity videoActivity) {
            super.handleMessage(msg, videoActivity);
            switch (msg.what) {
                case MSG_SHOW_PREVIEW:
                    showPreview();
                    break;
                case MSG_SEEK:
                    // 设置当前播放的位置
                    videoView.seekTo((int) msg.obj);
                    break;
                case MSG_UPDATE_SEEK_BAR:
                    watchPlayDurationAndSetSeekBar();
                    videoHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 1000);
                    break;
                default:
                    break;
            }
        }
    }

}