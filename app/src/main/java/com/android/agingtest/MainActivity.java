package com.android.agingtest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import static com.android.agingtest.VideoActivity.videoActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity测试：";

    private Button testButton;
    private EditText testTimeEdit;
    private RadioButton rb_4h;
    private RadioButton rb_8h;
    private CheckBox cb_video;

    private int testTotalTime = 4 * 60 * 60; // 4小时
    private Uri videoUri = null;
    // private String videoPath;
    public static boolean startCountDown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initVideoPath();

        rb_4h = findViewById(R.id.radio_4h);
        rb_8h = findViewById(R.id.radio_8h);
        cb_video = findViewById(R.id.cb_video_test);
        testButton = findViewById(R.id.testButton);
        testTimeEdit = findViewById(R.id.testTime);

        cb_video.setChecked(true);

        // 监听测试输入框文字的改变
        testTimeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence c, int i, int i1, int i2) {
                // 若输入了时间，则禁用选择时间的单选按钮
                if (c.length() > 0) {
                    rb_4h.setChecked(false);
                    rb_8h.setChecked(false);
                    rb_4h.setEnabled(false);
                    rb_8h.setEnabled(false);
                } else {
                    rb_4h.setEnabled(true);
                    rb_8h.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb_video.isChecked()) {

                    if (videoUri == null) {
                        Toast.makeText(MainActivity.this, "视频路径加载错误！", Toast.LENGTH_SHORT).show();
                        initVideoPath();
                        return;
                    }

                    String mtime = testTimeEdit.getText().toString().trim();
                    // 默认为4小时
                    if (mtime.equals("")) {
                        mtime = "4";
                    }
                    int hour = Integer.parseInt(mtime);
                    // 最少4小时
                    if (hour < 4) {
                        hour = 4;
                        Toast.makeText(MainActivity.this, "最少4小时！默认已设置为4小时", Toast.LENGTH_SHORT).show();
                    }
                    // 最长为240小时
                    if (hour > 240) {
                        hour = 240;
                        Toast.makeText(MainActivity.this, "最长240小时！已设置为最长240小时", Toast.LENGTH_SHORT).show();
                    }
                    if (rb_4h.isChecked()) {
                        hour = 4;
                    }
                    if (rb_8h.isChecked()) {
                        hour = 8;
                    }

                    // 真正测试时间(时—>秒)
                    testTotalTime = hour * 60 * 60;

                    if (Utils.isDebug) {
                        testTotalTime = 120;
                    }

                    // 开始播放视频，开始倒计时
                    playVideo();
                    startCountDown = true;
                } else {
                    Toast.makeText(MainActivity.this, "请选择你需要测试的项目！！！", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopVideo();
    }

    /**
     * 加载视频
     */
    private void initVideoPath() {
        // videoPath = "";

        String uri = "android.resource://" + getPackageName() + "/" + R.raw.knc_simplelife;
        videoUri = Uri.parse(uri);

        if (Utils.isDebug) {
            Log.w(TAG, "onActivityResult: " + videoUri);
        }
    }

    private void updateView() {
        if (startCountDown) {
            testTotalTime -= 1;
            if (testTotalTime > 0) {
                if (videoActivity != null) {
                    videoActivity.remainingTime.setText("剩余测试时间：" + Utils.calculatePlayTime(testTotalTime));
                }
                if (Utils.isDebug) {
                    Log.w(TAG, "updateView: 剩余时间：" + Utils.calculatePlayTime(testTotalTime));
                }
            } else {
                stopVideo();
                Toast.makeText(this, "测试完成！", Toast.LENGTH_SHORT).show();
                if (Utils.isDebug) {
                    Log.w(TAG, "updateView: ");
                }
                startCountDown = false;
            }
        }
    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // 要做的事情
            updateView();
            if (startCountDown) {
                // 每隔1秒执行一次
                handler.postDelayed(this, 1000);
            }
            if (Utils.isDebug) {
                Log.w(TAG, "run updateView()");
            }
        }
    };

    /**
     * 播放视频
     */
    private void playVideo() {
        Intent it = new Intent(MainActivity.this, VideoActivity.class);
        // it.putExtra("path", videoPath);
        it.putExtra("path", videoUri);
        it.putExtra("loop", true);
        try {
            startActivity(it);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 2秒钟之后开启计时
        handler.postDelayed(runnable, 2000);
    }

    /**
     * 停止播放视频
     */
    private void stopVideo() {
        try {
            if (videoActivity != null) {
                videoActivity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}